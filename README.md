Blackboard Learn Content Tweaker
===============================

version number: 0.0.2
author: Geoff Lee

Overview
--------

Tweak content in Blackboard Learn without API access

Installation / Usage
--------------------

Clone the repo:

    $ git clone https://github.com/gkluoe/tweakbb.git
    $ python setup.py install

Or, build with autopkg:

    $ autopkg repo-add https://github.com/gkluoe/tweakbb/autopkg-recipe
    $ autopkg run tweakbb.pkg

    
Contributing
------------

TBD

Example
-------

TBD
