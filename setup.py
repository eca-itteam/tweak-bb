from setuptools import setup, find_packages
from codecs import open
from os import path

__version__ = '0.0.2'

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='tweakbb',
    version=__version__,
    description='Tweak content in Blackboard Learn without API access',
    long_description=long_description,
    url='https://git.ecdf.ed.ac.uk/eca-itteam/tweak-bb',
    download_url='https://git.ecdf.ed.ac.uk/eca-itteam/tweak-bb/tarball/v' + __version__,
    license='Apache Software License',
    classifiers=[
      'Development Status :: 3 - Alpha',
      'Intended Audience :: Developers',
      'Programming Language :: Python :: 2',
    ],
    keywords='mac',
    packages=find_packages(exclude=['docs', 'tests*']),
    include_package_data=True,
    author='Geoff Lee',
    setup_requires=['pytest-runner', 'mechanize', 'bs4'],
    tests_require=['pytest', 'pylint'],
    author_email='g.lee@ed.ac.uk',
    entry_points={
        'console_scripts': [
           'tweakbb = tweakbb.__init__:main'
          ]
    },
)
