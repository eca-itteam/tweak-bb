# Copyright 2018 Geoff Lee
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""The tweakbb tool"""
from __future__ import print_function
import getpass
import urllib
import sys
import re
import argparse
import mechanize
from urlparse import urlparse, parse_qs
from bs4 import BeautifulSoup


VERSION = "0.0.2"
DESCRIPTION = """This is the tweakbb tool"""
BASEURL = 'https://www.learn.ed.ac.uk'

def main():
    """Main function - do useful stuff here!"""
    args = process_args()

    passwd = getpass.getpass(prompt='EASE Password for {}: '.format(args.USER), stream=None) 

    replacement_content = None
    if args.CONTENTFILE:
        with open(args.CONTENTFILE, 'r') as f:
            replacement_content = f.read()

    if args.COURSELIST:
        with open(args.COURSELIST, 'r') as f:
            course_list = f.read().rstrip('\n').split('\n')
    else:
        course_list = [ args.COURSE ]
        
    br = get_learn_browser(args.USER, passwd)

    # get a list of all my courses
    sys.stderr.write("Getting all courses...\n")
    my_courses = get_all_courses(br)

    if args.LISTCOURSES:
        # Just print my courses and exit
        for course in my_courses:
            sys.stderr.write("{}".format(course['id']))
        sys.exit(0)   

    for course in [ c for c in course_list if len(c) is not 0 ]:
        sys.stderr.write("Processing course {}\n".format(course))

        try:
            # Get the URL to the course we are dealing with
            course_url = course_url_from_course_id(br, my_courses, course)

            # Get the internal Learn ref for the course
            course_ref = get_course_ref(course_url)

            # Get the URL to the page containing the content
            page_url = find_page_in_course(br, course_url, args.PAGE)

            # Get the internal ID of the content
            content_id = find_content_id(br, course_url, page_url, args.TARGET)
        except BaseException as e:
            print("{}: {}".format(course, e))
            continue
    
        # if we have got this far, the content exists
        if args.VERIFY is True:
            print("{}: Content exists".format(course))
            continue

        if args.PRINT is True:
            # Print the content
            print_content_item(br, course_ref, content_id,)
        elif replacement_content is not None:
            edit_content_item(br, course_ref, content_id, replacement_content)

def process_args(argv=None):
    """Process any commandline arguments"""
    parser = argparse.ArgumentParser(description=DESCRIPTION,
                                     version=VERSION)
    parser.add_argument('--course', 
                        dest='COURSE', help=(('The NAME of the course which '
                                            'holds the content you wish to target')))
    parser.add_argument('--courselist', 
                        dest='COURSELIST', help=(('Path to a file which holds a list of names '
                                            'of courses whcih all hold the content you wish to target')))
    parser.add_argument('--page', 
                        dest='PAGE', help=(('The PAGE within the course on which '
                                          'the content you wish to target is displayed')))
    parser.add_argument('--target', 
                        dest='TARGET', help=(('The TITLE of the content item that you '
                                            'wish to edit')))  
    parser.add_argument('--newcontent', 
                        dest='CONTENTFILE', help=(('PATH to a file containing the replacement '
                                                 'content for the target')))
    
    parser.add_argument('--print-only', action='store_true', default=False,
                        dest='PRINT', help=('Just print target content: do not edit'))

    parser.add_argument('--verify-only', action='store_true', default=False,
                        dest='VERIFY', help=('Just print whether the target content exists or not'))

    parser.add_argument('--list-courses', action='store_true', default=False,
                        dest='LISTCOURSES', help=('Just list all my courses'))

    parser.add_argument('--user', 
                        dest='USER', help=('Username to authenticate to Learn '))

    args = parser.parse_args(argv)
    return args

def get_learn_browser(user, passwd):
    br = mechanize.Browser()
    br.set_handle_robots(False) 
    br.addheaders = [("User-agent", ("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:23.0)"
                                     "Gecko/20100101 Firefox/23.0"))]
    #br.set_debug_http(True)
    
    # authenticate to cosign
    br.open("https://www.ease.ed.ac.uk")

    r = br.open("https://www.ease.ed.ac.uk/cosign.cgi", 
                urllib.urlencode({'login': user, 'password': passwd}))

    # Log in to Learn
    resp = br.open("https://www.learn.ed.ac.uk/cgi-bin/login.cgi")

    return br

    
def course_url_from_course_id(browser, courses, course_id):
    ## Look for a link to a course named {course}
    ## and return it
    try:
        href = [ c['url'] for c in courses if c['id'] == course_id ][0]
        
        if href is []:
            raise BaseException("Failed to find a course called '{}'".format(course_id))
        else:
            return href
        return BASEURL + href
    except IndexError:
        raise BaseException("Failed to find a course called '{}'".format(course_id))


def get_all_courses(browser):
    """ Return a list of dicts, one for each course to which I have access. """
    course_list = ('https://www.learn.ed.ac.uk/webapps/blackboard/execute/viewCatalog?'
                   'editPaging=false&showAllMax=1000&moduleId=_4_1&searchOperator=NotBlank&showAll=true'
                   '&command=SavedSearch&sortCol=identifier&type=MyCourses&sortDir=ASCENDING&startIndex=0'
                   '&searchField=CourseId')
    browser.open(course_list)
    resp = browser.open(course_list)
    page = BeautifulSoup(resp.read(), features="html5lib")
    rows = page.find_all('tr', {'id': re.compile(r'listContainer_row:')})
    courses = [ { 'id': row.th.a.contents[0].lstrip(), # Course ID
                  'title': row.td.find_all('span')[1].string, # Course Title
                  'url': BASEURL + row.th.a['href'] } for row in rows ] # URL to course 
    return courses



def get_course_ref(url):
    sys.stderr.write("Looking for internal ref in URL {}\n".format(url))
    qs = urlparse(url)[4] # Parse out query string
    id = parse_qs(qs)['course_id'][0]
    sys.stderr.write("Course internal ref is: {}\n".format(id))
    return id


def find_page_in_course(browser, course_url, page):
    sys.stderr.write("Searching for page {} in course {}...\n".format(page, course_url))
    course = browser.open(course_url)
    haystack = BeautifulSoup(course.read(), features="html5lib")
    href = None
    for span in haystack.find_all('span'):
        if span.string == page:  
            href = span.parent['href']
            sys.stderr.write("Got it: {}\n".format(href))
    
    if href is None:
        raise BaseException("Failed to find a page called '{}'\n".format(page))
    else:
        return href
        

def find_content_id(browser, course, page_url, target):
    sys.stderr.write("Searching for ID of content item called '{}'\n".format(target))
    content_page = browser.open(page_url)
    haystack = BeautifulSoup(content_page.read(), features="html5lib")
    
    # The content ID can be gleaned from a <div> of class 'item clearfix'
    # which contains a span containing the text of the title of the 
    # target content
    the_id = None

    # Get all <div> of class 'item clearfix' - each of these is a content item
    content_items = haystack.find_all('div', {'class': 'item clearfix'})
    sys.stderr.write("found {} content items\n".format(len(content_items)))

    # Search each of them for a <span> which matches our target
    for item in content_items:
        spans = item.find_all('span')
        for span in spans:
            if span.string == target:
                # If we found the target, the ID we want is that of the 
                # enclosing <div>
                the_id = item["id"]

    if the_id is None:
        raise BaseException('Failed to get content ID for item entitled "{}"'.format(target))
    else:
        return the_id



def edit_content_item(br, course_id, content_id, replacement_content):
    edit_url = ("https://www.learn.ed.ac.uk/webapps/blackboard/execute/manageCourseItem?"
                "content_id={}&course_id={}&dispatch=edit").format(content_id, course_id)

    sys.stderr.write("Content edit URL is: {}\n".format(edit_url))

    br.open(edit_url)
    br.select_form(id='the_form')
    br.form['htmlData_text'] = replacement_content
    resp = br.submit()
    if resp.code is 200:
        sys.stderr.write("That seemed to work OK\n")


def print_content_item(br, course_id, content_id):
    edit_url = ("https://www.learn.ed.ac.uk/webapps/blackboard/execute/manageCourseItem?"
                "content_id={}&course_id={}&dispatch=edit").format(content_id, course_id)
    br.open(edit_url)
    br.select_form(id='the_form')
    print(br.form['htmlData_text'])


if __name__ == "__main__":
    main()
